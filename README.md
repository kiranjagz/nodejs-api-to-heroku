# nodejs-api-to-heroku

NodeJs application to deploy to heruko, using ci-cd.
Project built using: 
1. nodejs
2. mocha and chai test framework
3. deploy to heroku
4. api tests using Postman
5. reports hosted on gitlab pages

# Postman Api pages link

https://kiranjagz.gitlab.io/nodejs-api-to-heroku/
